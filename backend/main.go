package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	// we use handlers.LoggingHandler to log incoming http reqs in standard apache format
	"github.com/gorilla/handlers"

	"github.com/rs/cors"

	// logging framework
	log "github.com/sirupsen/logrus"
	"go.elastic.co/apm/module/apmgorilla"

	// used for rotating logfiles
	"gopkg.in/natefinch/lumberjack.v2"
)

/*
 * I've implented 2 loggers:
 *  - an application logger mostly used for debug-level logging.
 *    This log is meant to be machine readable from tools like elasticsearch
 *  - an apache access logger
 */

// Application logger
var Logger = log.New()

/*
 * rolling file for http logger. The logger is initialized in main(),
 * leveraging mux middleware
 */
var httpLoggerOutputFile = &lumberjack.Logger{
	Filename:   "/var/log/empamini/access.log",
	MaxSize:    500, // megabytes
	MaxBackups: 3,
	MaxAge:     7,    //days
	Compress:   true, // disabled by default
}

// boilerplate to be able to use router.Use()
func loggingMiddleware(next http.Handler) http.Handler {
	return handlers.LoggingHandler(httpLoggerOutputFile, next)
}

func main() {

	// application logger init
	Logger.Formatter = &log.JSONFormatter{}
	Logger.Level = log.DebugLevel
	Logger.Out = &lumberjack.Logger{
		Filename:   "/var/log/empamini/app.log",
		MaxSize:    500, // megabytes
		MaxBackups: 3,
		MaxAge:     7,    //days
		Compress:   true, // disabled by default
	}

	/*
	 * Logger.WithFields() allows to generate structured log entries.
	 * Where applicable, the log sructure is:
	 *  - module: go source file name
	 *  - method: go method name
	 *  - user: username of the user
	 *  - action: action attempted by the user
	 *  - result: result of the action {success | failure}
	 */
	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Info("Starting backend")
	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Debug("Initializing Router")

	r := mux.NewRouter()
	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Debug("Injecting instrumentation")

	// enable elastic apm go agent
	r.Use(apmgorilla.Middleware())

	// enable http access logging
	r.Use(loggingMiddleware)

	r.HandleFunc("/ping", PingHandler).Methods("GET")
	r.HandleFunc("/login", LoginHandler).Methods("POST")
	r.Handle("/users/{userId}", tokenValidation(http.HandlerFunc(GetUserHandler))).Methods("GET")

	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Debug("Router initialized")
	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Debug("Setting up CORS")
	c := cors.New(cors.Options{
		AllowedOrigins:     []string{"*"},
		AllowedMethods:     []string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:     []string{"X-Requested-With", "Content-Type", "content-type", "Origin", "Accept", "Authorization"},
		OptionsPassthrough: false,
	})

	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Info("Listen on port 9000")
	handler := c.Handler(r)
	Logger.WithFields(log.Fields{"module": "main", "method": "main"}).Fatal(http.ListenAndServe(":9000", handler))
}

func tokenValidation(next http.Handler) http.Handler {

	Logger.WithFields(log.Fields{"module": "main", "method": "tokenValidation"}).Debug("validating token")

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var token = r.Header.Get("Authorization")
		if token == "" {
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode("missing token")
			Logger.WithFields(log.Fields{"module": "main", "method": "tokenValidation", "action": "tokenValidation", "result": "failure"}).Debug("No authentication token")
			return
		}

		var paramUserID = mux.Vars(r)[userIDKey]
		userID, err := strconv.ParseInt(paramUserID, 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(fmt.Sprintf("user id not valid : %v", err)))
			Logger.WithFields(log.Fields{"module": "main", "method": "tokenValidation", "action": "tokenValidation", "user": paramUserID, "result": "failure"}).Debug("Invalid userid. Parsing to Int failed")
			return
		}

		ua := getUserAccount(userID)
		if ua == nil || ua.Token != token {
			w.WriteHeader(http.StatusForbidden)
			json.NewEncoder(w).Encode("invalid token")
			Logger.WithFields(log.Fields{"module": "main", "method": "tokenValidation", "action": "tokenValidation", "user": paramUserID, "result": "failure"}).Debug("Invalid token")
			return
		}

		next.ServeHTTP(w, r)
	})
}
