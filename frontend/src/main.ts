import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import { init as initApm } from 'elastic-apm-js-base'


/*
 * Initialize the elaetic apm agent.
 * By default this agent only automatically profiles page loads.
 * We will add some custom instrumentation in the login component.
 */
var apm = initApm({

  serviceName: 'empamini-frontend',
  serverUrl: 'http://localhost:8200',
  serviceVersion: '1.0',
  logLevel: 'debug',
  pageLoadTransactionName: 'pageLoad',
  distributedTracingOrigins: ['http://localhost']

});


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
