import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { MatSnackBar } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "../auth.service";

import { init as initApm } from 'elastic-apm-js-base'


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  private email: string;
  private password: string;
  private apm;


  constructor(
    private auth: AuthService,
    private api: ApiService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {

    /*
     * init apm agent. Will add custom instrumentation to login
     */
    this.apm = initApm({

      serviceName: 'empamini-frontend',
      serverUrl: 'http://localhost:8200',
      serviceVersion: '1.0',
      logLevel: 'debug',
      pageLoadTransactionName: 'pageLoad',
      distributedTracingOrigins: ['http://localhost']

    });

    if (window.localStorage.getItem("token")) {
      this.redirect();
      return;
    }
  }

  ngOnInit() { }

  check(): boolean {
    return Boolean(this.email) && Boolean(this.password);
  }

  do() {

    if (!this.check()) {
      this.snackBar.open("Please fill the missing fields");
      return;
    }

    /*
     * Custom instrumentation - very basic, just defines a login transaction
     */
    var transaction = this.apm.startTransaction('Login transaction', 'Login');
    /* 
     * This span represent the request to the backend API
     */
    var loginSpan = transaction.startSpan('Login POST span', 'http');

    this.api.login(this.email, this.password).subscribe(
      (response: any) => {

        /*
         * If the api request was succesful, close the span and the transaction
         */
        loginSpan.end();
        

        this.setToken(response.token);
        var redirectSpan = transaction.startSpan('Login redirect span', 'http');
        this.redirect();
        redirectSpan.end();
        transaction.end()
      },
      err => {

        /*
         * If the api request failed, capture the error using the apm agent
         */
        this.apm.captureError(new Error(`ERROR: status: ${err.status}, message: ${err.message}`))
        this.snackBar.open("Invalid login");
      }
    );
  }

  redirect() {
    this.router.navigate(["/home"]);
  }

  setToken(token: string) {
    this.auth.setToken(token);
  }
}
