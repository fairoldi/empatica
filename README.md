# Empatica Web Demo

## How to deploy
docker-compose up

## Accessing application
docker-compose will expose 

 - frontend on localhost:80
 - backend on localhost:9000 

## Accessing Kibana
docker-compose will expose kibana on localhost:5601

Relevant dashboards are:

 - [Metricbeat System] Overview
 - [Filebeat Apache2] Access and error logs
 - [APM] Services
 - [Metricbeat Docker] Overview

The APM section can be used to visualize waterfall diagrams 
and inspect transactions.

Backend log files can be inspected using the "Discover" section

On the first access, you will be promted to select a default index.
Just click on filebeat-* and then on the little star in the top right corner

## What's new

### Backend
 - use glide to manage go dependencies
 - added application-level logging usild logrus
 - added apache2 access logging using mux middleware
 - added elastic apm instrumentation
 - created dockerfile to build and run as a container

### Frontend
 - added elastic apm instrumentation
 - created dockerfile to build and run as a container
 - nginx now proxies localhost/api -> backend:9000/

### Log management
 - backend logs are ingested in elk via filebeat

### Perf. monitoring
 - container metrics imported via metricbeat
 - docker host metrics imported via metricbeat

### APM instrumentation
 - instrumented frontend with elastic javascript (Real User Monitoring) agent
 - instrumented backend with elastic go agent
 - distributed tracing is now working. Unfortunately the Kibana UI is not very mature, 
   but trace.id is correctly propagated in elastic and ca be used in Discover
   e.g., by searching trace.id:TRACEID , and this will show all APM events, across 
   both tiers

### Kibana
 - sample dashboards for apm, filebeat and metricbeat are automatically deployed

## Notes
The FE application is now server trhough an nginx http server which is configured
to act as a reverse proxy to the backend app.


In a real-world scenario there would be 3 separate networks:

    - internet facing end-user access
    - frontend - backend communication
    - management / monitoring

Given how simple the sample application was I didn't bother for the time being.


I decided to use docker-compose for simplicity, and because I didn't have a load balancer set up in my
kubernetes environment.

The docker-compose file could be easily converted to kubernetes resource files using kompose. 
A few considerations on this:

 - filebeats should probably run as a sidecar to the frontend container
 - metricbeats should run in a daemonset
 - both frontend and backend would be implemented as deployments, and exposed via loadbalancer, possibly with autoscaling 